; <?php/*
[general]
name                        ="editlinks"
version                     ="0.9.8"
encoding                    ="UTF-8"
mysql_character_set_database="latin1,utf8"
description                 ="Add publications links to articles and topics on all page of the site"
description.fr              ="Module permettant d'ajouter des liens d'accès direct aux interfaces de publication"
long_description.fr         ="README.md"
delete                      =1
ov_version                  ="8.4.94"
php_version                 ="5.3.0"
addon_access_control        =1
author                      ="Cantico"
icon                        ="icon.jpg"
tags                        ="library,default"

[addons]

widgets=">=1.0.90"
jquery=">=1.4.4.90"
sitemap_editor=">=0.8.4"

;*/?>