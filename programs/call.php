<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
bab_functionality::includeOriginal('Icons');


/**
 * Transform action to object exportable in json
 * @param Widget_Action $action
 */
function editlinks_transformAction(Widget_Action $action)
{
	$a = new stdClass();
	$a->url = bab_convertStringFromDatabase($action->url(), 'UTF-8');
	$a->icon = $action->getIcon();
	$a->title = bab_convertStringFromDatabase($action->getTitle(), 'UTF-8');
	return $a;
}



function editlinks_getJSON()
{
    $url = new bab_url(bab_gp('url'));

    // build JSON object

    $json = array();

    $actions = bab_rp('actions', array());

    foreach ($actions as $funcName => $elements) {
        $contextActions = bab_functionality::get('ContextActions/'.$funcName);
        /*@var $contextActions Func_ContextActions */
        foreach ($elements as $classes) {
            $json[] = array(
                'classes' => $classes,
                'actions' => array_map('editlinks_transformAction', $contextActions->getActions($classes, $url))
            );
        }
    }

    return json_encode($json);

}

die(editlinks_getJSON());
