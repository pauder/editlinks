/**
 * This append button to the body
 * @param element
 * @param classname
 * @returns {___anonymous276_281}
 */
function appendButton(parent, element, classname, title, link)
{
	var iconlayer = document.createElement('SPAN');
	iconlayer.className = 'icon-left-16 icon-left icon-16x16';
	parent.appendChild(iconlayer);
	
	var button = document.createElement('SPAN');
	button.className = 'editlinks-button icon '+classname;
	
	iconlayer.appendChild(button);
	
	button.setAttribute('title', title);
    button.onclick = function() {
        //ADD "/" to fix issue with IE & Edge
        document.location.href = '/'+link;
    };
    
    jQuery(button).data('editlinks-element', element);
	
	return button;
}


/**
 * Get main item from mouse event element
 * @param domElement
 */
function editlinks_getEventItem(domElement) {
	var item = jQuery(domElement);
	
	if (item.hasClass('editlinks-button')) {
		item = item.data('editlinks-element');
	} 
	
	return item;
}


function editlinks_mouseover()
{
	
	var timeoutId;
	var item = editlinks_getEventItem(this);
	
    
	if (timeoutId = item.data('editlinks-timeoutId')) {
		window.clearTimeout(timeoutId);
	}
    
    var buttons = jQuery(item.data('editlinks-buttons'));
    
    buttons.show();
    
    buttons.each(function(pos, button) {
    	
	    var offset = pos * 27;
	
	    jQuery(button).position({
	    	of: item,
	    	my: 'left+'+offset+' bottom',
	    	at: 'left top'
	    });
    });
    
    
};


function editlinks_mouseout()
{
	var item = editlinks_getEventItem(this);
	var buttons;
	
	buttons = jQuery(item.data('editlinks-buttons'));
	
	item.data('editlinks-timeoutId', window.setTimeout(function() {
		buttons.fadeOut(400);
	}, 100));
    
    
}


/**
 * add edit button on content
 */
jQuery.fn.addEditlinksButton = function(link, classname, title)
{
	
    this.each(function(i, element) {
    	
    	element = jQuery(element);
    	

    	var button;
        var parent;
        
        if (element.text())
        {
        	// There is content, display button on mouse over
        	
        	element.addClass('editlinks-item');
        	
        	parent = document.getElementsByTagName('BODY')[0];
        	button = appendButton(parent, element, classname, title, link);
        	button.className += ' editlinks-hoverbutton';
        	

            element.add(button).mouseenter(editlinks_mouseover);
            element.add(button).mouseleave(editlinks_mouseout);
            
            jQuery(button).hide();
            
        } else {
        	
        	// no content, display button in flux
        	
        	button = appendButton(element.get(0), element, classname, title, link);
        	button.className += ' editlinks-staticbutton';
        }
        
        var buttons = element.data('editlinks-buttons');
        if (undefined === buttons) {
        	buttons = jQuery(button);
        } else {
        	buttons = buttons.add(button);
        }

        element.data('editlinks-buttons', buttons);
    });
};



function editlinks_getButtonsQuery() {
	var ctxAct = window.babAddonWidgets.getMetadata('editlinksSelectors');
	
	if (!ctxAct) {
		return null;
	}
	
	var result = {};
	var size = 0; // size limit for the URL length
	
	for (var funcName in ctxAct) {
		if (ctxAct.hasOwnProperty(funcName)) {
			var list = [];
			var minPerFunction = 0;
			
			jQuery(ctxAct[funcName]).each(function() {
				
				if (size > 3000 && minPerFunction > 10) {
					return;
				}
				
				var values = this.className.split(/\s+/);
				for( var i=0; i<values.length; i++) {
					size += (funcName.length + values[i].length);
					minPerFunction++;
				}
				list.push(values);
		    });
			
			result[funcName] = list;
			
			if (size > 4000) {
				break;
			}
		}
	}
	
	return { 
		actions: result,
		url: document.location.href
	};
}



jQuery(document).ready(function() {
	
	jQuery.getJSON('?addon=editlinks.call', editlinks_getButtonsQuery(), function(data) {
		for (var i=0; i<data.length; i++) {
			var element = jQuery('.'+data[i].classes.join('.'));
			var actions = data[i].actions;
			
			for (var j=0; j<actions.length; j++) {
				var a = actions[j];
				element.addEditlinksButton(a.url, a.icon, a.title);
			}
		}
	});

});