// loaded on page refresh


window.babAddonEditlinks = new Object();



/**
 * @param	string	prefix
 * return Array
 */
window.babAddonEditlinks.search = function(prefix, search, cb)
{
    jQuery('[class*="'+prefix+'"]').each(function(k, item) {
        var m = jQuery(item).attr('class').match(search);
        if (null !== m) {
            cb(m);
        }
    });
};



/**
 * @param	string	prefix
 * return Array
 */
window.babAddonEditlinks.getIdsByPrefix = function(prefix)
{
    var arr = new Array();
    var search = new RegExp(prefix+'([0-9]+)');

    window.babAddonEditlinks.search(prefix, search, function(m) {
        var id = parseInt(m[1]);
        if (-1 == jQuery.inArray(id, arr))
        {
            arr.push(id);
        }
    });

    return arr;
};


/**
 * @param	string	prefix
 * return Array
 */
window.babAddonEditlinks.getDynamicNodes = function(prefix)
{
    var list = {};
    var search = new RegExp(prefix+'([^\-]+)-(.+)');

    window.babAddonEditlinks.search(prefix, search, function(m) {

        var structure = m[1];

        if (undefined === list[structure]) {
            list[structure] = [];
        }

        var id = m[2];

        if (-1 == jQuery.inArray(id, list[structure]))
        {
            list[structure].push(id);
        }
    });

    return list;
};




/**
 * @param	string	prefix
 * return Array
 */
window.babAddonEditlinks.getStringsByPrefix = function(prefix)
{
    var arr = new Array();
    var search = new RegExp(prefix+'(.+)');

    window.babAddonEditlinks.search(prefix, search, function(m) {
        if (-1 == jQuery.inArray(m[1], arr))
        {
            arr.push(m[1]);
        }
    });

    return arr;
};




window.babAddonEditlinks.mouseover = function(item, link, classname, title)
{
    if (item.find('.'+classname).length > 0)
    {
        return;
    }

    item.addClass('editlinks-item-hover');
    var button = jQuery('<span></span>');
    item.append(button);
    button.addClass('editlinks-button');
    button.addClass('editlinks-hoverbutton');
    button.addClass(classname);
    button.attr('title', title);
    button.click(function() {
        document.location.href = link;
    });



    if (button.prev().is('.editlinks-button'))
    {
        var left = (button.prev().position().left + 20);
        button.css('left', left+'px');
    }
};



/**
 * add edit button on content
 */
jQuery.fn.addEditlinksButton = function(link, classname, title)
{
    this.each(function(i, element) {

        element = jQuery(element);

        if (element.text())
        {

            element.hover(
                function() {
                    var item = jQuery(this);
                    window.babAddonEditlinks.mouseover(item, link, classname, title);
                },
                function() {
                    var item = jQuery(this);
                    item.removeClass('editlinks-item-hover');
                    item.find('.editlinks-button').each( function () {
                        if(jQuery(this).parent('div').length) {
                            jQuery(this).remove();
                        }
                    });
                }
            );

        } else {

            var button = jQuery('<span></span>');
            element.append(button);
            button.addClass('editlinks-button');
            button.addClass('editlinks-staticbutton');
            button.addClass(classname);
            button.attr('title', title);
            button.click(function() {
                document.location.href = link;
            });
        }
    });
};






window.babAddonEditlinks.addArrLinks = function(item, classPrefix, arrId, className, urlPattern, labelPattern)
{
    if (undefined === arrId) {
        return;
    }

    var url, labelStr;

    for(var i = 0; i < arrId.length; i++) {

        url = urlPattern(arrId[i]);
        labelStr = labelPattern(arrId[i]);

        jQuery('.'+classPrefix+arrId[i]).addEditlinksButton(url, className, labelStr);

        if (item.hasClass(classPrefix+arrId[i])) {
            window.babAddonEditlinks.mouseover(item, url, className, labelStr);
        }
    }
};




window.babAddonEditlinks.addObjLinks = function(item, classPrefix, list, className, urlPattern, labelPattern)
{
    if (undefined === list) {
        return;
    }

    var url, labelStr, value;

    for(var prop in list) {

        if (!list.hasOwnProperty(prop)) {
            continue;
        }

        for(var i=0; i<list[prop].length; i++) {

            value = list[prop][i];
            classSuffix = value;

            if (value instanceof Object) {
                classSuffix = value.id;
            }

            url = urlPattern(prop, value);
            labelStr = labelPattern(prop, value);
            jQuery('.'+classPrefix+prop+'-'+classSuffix).addEditlinksButton(url, className, labelStr);

            if (item.hasClass(classPrefix+prop+'-'+classSuffix)) {
                window.babAddonEditlinks.mouseover(item, url, className, labelStr);
            }
        }
    }
};





window.babAddonEditlinks.init = function(item, ids)
{
    if ( typeof this.done == 'undefined' ) {
        this.done = false;
    }

    if (!this.done)
    {

        var parameters = ids;
        parameters.url = document.location.href;

        jQuery.ajax({
            url : '?addon=editlinks.call',
            //method : 'POST',
            async : true,
            dataType: 'json',
            data : parameters,
            success : function(data) {

                if (null == data)
                {
                    return;
                }

                window.babAddonEditlinks.addArrLinks(
                    item,
                    'bab-article-',
                    data.articles,
                    'editlinks-edit',
                    function(id) {
                        return '?tg=articles&idx=Modify&article='+id;
                    },
                    function() {
                        return data.label.articles;
                    }
                );

                window.babAddonEditlinks.addArrLinks(
                    item,
                    'bab-articletopic-',
                    data.topicssub,
                    'editlinks-add',
                    function(id) {
                        return '?tg=articles&idx=Submit&topics='+id;
                    },
                    function() {
                        return data.label.topicssub;
                    }
                );

                window.babAddonEditlinks.addArrLinks(
                    item,
                    'bab-articletopic-',
                    data.topicsman,
                    'editlinks-man',
                    function(id) {
                        return '?tg=topman&idx=Articles&item='+id;
                    },
                    function() {
                        return data.label.topicsman;
                    }
                );

                window.babAddonEditlinks.addArrLinks(
                    item,
                    'publication-pubnode-',
                    data.pubnodes,
                    'editlinks-add',
                    function(id) {
                        return '?tg=addon/publication/main&idx=publication.edit&pubnode='+id;
                    },
                    function(id) {
                        return data.label.pubnodes[id];
                    }
                );


                window.babAddonEditlinks.addObjLinks(
                    item,
                    'publication-dynamicrecord-',
                    data.dynamicrecords,
                    'editlinks-edit',
                    function(structure, publication) {
                        if (undefined !== publication.nodes[0]) {
                            var node = publication.nodes[0];
                            return '?node='+node.id+'&tg=addon%2Fsitemap_editor%2Fmain&idx=node.edit&explorer=0&backurl='+encodeURIComponent(document.location.href);
                        }

                        return '?tg=addon/publication/main&idx=publication.edit&structure='+encodeURIComponent(structure)+'&id='+publication.id;
                    },
                    function(structure) {
                        return data.label.dynamicrecords[structure];
                    }
                );

                //MAIL
                if (data.mail) {
                    for(var structure in data.mail) {
                        for(var prop in data.mail[structure]) {
                            var mail = data.mail[structure][prop];
                            var url = '?tg=addon/publication/main&idx=publication.mail&structure='+encodeURIComponent(structure)+'&id='+mail.id;
                            jQuery('.publication-dynamicrecord-'+structure+'-'+mail.id).addEditlinksButton(url, 'editlinks-mail', data.label.mail);
                        }
                    }
                }


                window.babAddonEditlinks.addObjLinks(
                    item,
                    'publication-nodedynamicrecord-',
                    data.nodedynamicrecords,
                    'editlinks-add',
                    function(structure, id) {
                        return '?tg=addon/sitemap_editor/main&idx=node.add&content_type=datastructure%2F'+encodeURIComponent(structure)+'&parentNode='+id+'&explorer=0&backurl='+encodeURIComponent(document.location.href);
                    },
                    function(structure) {
                        return data.label.nodedynamicrecords[structure];
                    }
                );


                window.babAddonEditlinks.addArrLinks(
                    item,
                    'smed-sitemapnode-',
                    data.sitemapnodes,
                    'editlinks-man',
                    function(id) {
                        return '?tg=addon/sitemap_editor/main&node.display&node='+id;
                    },
                    function() {
                        return data.label.sitemapnodes;
                    }
                );

            }
        });

    }

    this.done = true;
};




jQuery(document).ready(function() {

    var ids = {
        articles: window.babAddonEditlinks.getIdsByPrefix('bab-article-'),
        topics: window.babAddonEditlinks.getIdsByPrefix('bab-articletopic-'),
        pubnodes: window.babAddonEditlinks.getIdsByPrefix('publication-pubnode-'),
        dynamicrecords: window.babAddonEditlinks.getDynamicNodes('publication-dynamicrecord-'), // objects
        nodedynamicrecords: window.babAddonEditlinks.getDynamicNodes('publication-nodedynamicrecord-'), // objects
        sitemapnodes: window.babAddonEditlinks.getStringsByPrefix('smed-sitemapnode-')
    };

    function initElement(className) {

        var block = jQuery('.'+className);

        if (block.children().length === 0 && jQuery.trim(block.text()) === "" ) {
            window.babAddonEditlinks.init(block, ids);
        }

        block.hover(function() {
            window.babAddonEditlinks.init(jQuery(this), ids);
        });
    }


    var i = 0, arr;

    // init script on mouse hover if content in bloc

    for(i = 0; i < ids.articles.length; i++) {
        initElement('bab-article-'+ids.articles[i]);
    }

    for(i = 0; i < ids.topics.length; i++) {
        initElement('bab-articletopic-'+ids.topics[i]);
    }

    for(i = 0; i < ids.pubnodes.length; i++) {
        initElement('publication-pubnode-'+ids.pubnodes[i]);
    }

    for(var structure in ids.dynamicrecords) {

        arr = ids.dynamicrecords[structure];

        for(i =0; i< arr.length; i++) {
            initElement('publication-dynamicrecord-'+structure+'-'+arr[i]);
        }
    }

    for(var structure in ids.nodedynamicrecords) {

        arr = ids.nodedynamicrecords[structure];

        for(i =0; i< arr.length; i++) {
            initElement('publication-nodedynamicrecord-'+structure+'-'+arr[i]);
        }
    }

    for(i = 0; i < ids.sitemapnodes.length; i++) {
        initElement('smed-sitemapnode-'+ids.sitemapnodes[i]);
    }

});













